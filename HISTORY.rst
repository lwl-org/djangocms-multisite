.. :changelog:

History
-------

0.3.0 (2023-06-06)
++++++++++++++++++

* Django 2.2 / django CMS 3.7 support
* Dropped older Django / django CMS versions


0.2.0 (unreleased)
++++++++++++++++++

* Django 1.11 / django CMS 3.4+ support
* Dropped older Django / django CMS versions
* Improved documentation

0.1.0 (unreleased)
++++++++++++++++++

* First experimental release
